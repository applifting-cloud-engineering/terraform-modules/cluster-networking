## Gke-cluster-networking

Module for calculating subnet IP address prefixes. 

The module assign a network address prefix in CIDR notation that all of the requested subnetwork prefixes will be allocated within with the argument `base_cidr_block` and `networks` for describing requested subnetwork prefixes.

The module creates subnets for `nodes`, `pods`, `services`, `dataplane`.

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_clusters"></a> [clusters](#input\_clusters) | Clusters where you want to use this module | `any` | n/a | yes |
| <a name="input_dataplane_subnet"></a> [dataplane\_subnet](#input\_dataplane\_subnet) | Subnet for dataplane | `string` | `"192.168.1.0/24"` | no |
| <a name="input_mask_new_bits"></a> [mask\_new\_bits](#input\_mask\_new\_bits) | Number of additional network prefix bits to add | `number` | `4` | no |
| <a name="input_nodes_subnet"></a> [nodes\_subnet](#input\_nodes\_subnet) | Subnet for nodes | `string` | `"10.1.0.0/16"` | no |
| <a name="input_pods_subnet"></a> [pods\_subnet](#input\_pods\_subnet) | Subnet for pods | `string` | `"172.16.0.0/16"` | no |
| <a name="input_services_subnet"></a> [services\_subnet](#input\_services\_subnet) | Subnet for services | `string` | `"172.20.0.0/16"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_dataplanes"></a> [dataplanes](#output\_dataplanes) | Variable for calling network_cidr_blocks for dataplanes |
| <a name="output_nodes"></a> [nodes](#output\_nodes) | Variable for calling network_cidr_blocks for nodes |
| <a name="output_pods"></a> [pods](#output\_pods) | Variable for calling network_cidr_blocks for pods |
| <a name="output_services"></a> [services](#output\_services) | Variable for calling network_cidr_blocks for services |

## Usage

Calling module gke-cluster-networking

```hcl
module "cidrs" {
  source = "gitlab.com/applifting-cloud-engineering/gke-cluster-networking/src"
  version = "0.1.0"

  clusters = ["<CLUSTER_NAME1>", "<CLUSTER_NAME2>"]
}
```

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_dataplane"></a> [dataplane](#module\_dataplane) | hashicorp/subnets/cidr | 1.0.0 |
| <a name="module_nodes"></a> [nodes](#module\_nodes) | hashicorp/subnets/cidr | 1.0.0 |
| <a name="module_pods"></a> [pods](#module\_pods) | hashicorp/subnets/cidr | 1.0.0 |
| <a name="module_services"></a> [services](#module\_services) | hashicorp/subnets/cidr | 1.0.0 |
