module "nodes" {
  source  = "hashicorp/subnets/cidr"
  version = "1.0.0"

  base_cidr_block = var.nodes_subnet
  networks = [
    for subnet in var.clusters :
    {
      name     = subnet
      new_bits = var.mask_new_bits
    }
  ]
}


module "pods" {
  source  = "hashicorp/subnets/cidr"
  version = "1.0.0"

  base_cidr_block = var.pods_subnet
  networks = [
    for subnet in var.clusters :
    {
      name     = subnet
      new_bits = var.mask_new_bits
    }
  ]
}

module "services" {
  source  = "hashicorp/subnets/cidr"
  version = "1.0.0"

  base_cidr_block = var.services_subnet
  networks = [
    for subnet in var.clusters :
    {
      name     = subnet
      new_bits = var.mask_new_bits
    }
  ]
}

module "dataplane" {
  source  = "hashicorp/subnets/cidr"
  version = "1.0.0"

  base_cidr_block = var.dataplane_subnet
  networks = [
    for subnet in var.clusters :
    {
      name     = subnet
      new_bits = 4
    }
  ]
}
