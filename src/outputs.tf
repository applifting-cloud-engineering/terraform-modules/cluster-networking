output "network_cidr_blocks_nodes" {
  value       = module.nodes.network_cidr_blocks
  description = "Variable for calling network_cidr_blocks for nodes"
}

output "network_cidr_blocks_pods" {
  value       = module.pods.network_cidr_blocks
  description = "Variable for calling network_cidr_blocks for pods"
}

output "network_cidr_blocks_services" {
  value       = module.services.network_cidr_blocks
  description = "Variable for calling network_cidr_blocks for services"
}

output "network_cidr_blocks_dataplanes" {
  value       = module.dataplane.network_cidr_blocks
  description = "Variable for calling network_cidr_blocks for dataplanes"
}
