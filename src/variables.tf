# Clusters where you want to use this module
variable "clusters" {}

variable "nodes_subnet" {
  description = "Subnet for nodes"
  type        = string
  default     = "10.1.0.0/16"
}

variable "pods_subnet" {
  description = "Subnet for pods"
  type        = string
  default     = "172.16.0.0/16"
}

variable "services_subnet" {
  description = "Subnet for services"
  type        = string
  default     = "172.20.0.0/16"
}

variable "dataplane_subnet" {
  description = "Subnet for dataplane"
  type        = string
  default     = "192.168.1.0/24"
}

variable "mask_new_bits" {
  description = "Number of additional network prefix bits to add"
  type        = number
  default     = 4
}
